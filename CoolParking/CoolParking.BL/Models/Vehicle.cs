﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private readonly string _id;
        private readonly VehicleType _vehicleType;
        private decimal _balance;
        
        public string Id { get => _id; }
        public VehicleType VehicleType { get => _vehicleType; }
        public decimal Balance { get => _balance; set => _balance = value; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            _id = IsIdFormatCorrect(id) ? id : throw new ArgumentException("Invalid identifier format");
            _balance = balance >= 0 ? balance : throw new ArgumentException("Balance cannot be negative");
            _vehicleType = vehicleType;
        }

        public static bool IsIdFormatCorrect(string id)
        {
            return Regex.IsMatch(id, @"^[a-zA-Z]{2}-\d{4}-[a-zA-Z]{2}$");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var example = "XX-YYYY-XX";
            var result = new char[example.Length];
            var rand = new Random();
            for (int i = 0; i < example.Length; i++)
            {
                if (example[i] == 'X')
                {
                    result[i] = (char) rand.Next('a', 'z');
                    continue;
                }

                if (example[i] == 'Y')
                {
                    result[i] = (char) rand.Next(48, 57);
                    continue;
                }

                result[i] = '-';
            }

            return result.ToString();
        }
        
    }
}