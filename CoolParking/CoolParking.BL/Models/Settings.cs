﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingStartBalance { get; set; } = 0;
        public static int ParkingCapacity { get; set; } = 10;
        public static double WithdrawInterval { get; set; } = 5;
        public static double LogInterval { get; set; } = 60;

        public static Dictionary<VehicleType, decimal> TariffDependingOnTransportType { get; set; } 
            = new Dictionary<VehicleType, decimal>
        {
            {VehicleType.PassengerCar, 2m},
            {VehicleType.Truck, 5m},
            {VehicleType.Bus, 3.5m},
            {VehicleType.Motorcycle, 1m}
        };

        public static decimal FineMultiplier { get; set; } = 2.5m;
    }
}