﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    class Parking
    {
        internal decimal Balance { get; set; } = Settings.ParkingStartBalance;
        internal int Capacity { get; } = Settings.ParkingCapacity;
        internal readonly List<Vehicle> Vehicles = new List<Vehicle>();
        internal readonly List<string> VehicleIds = new List<string>();
        internal readonly List<TransactionInfo> Transactions = new List<TransactionInfo>();
        internal int LastLoggedTransactionIndex { get; set; } = 0;
        internal List<TransactionInfo> LastTransactions = new List<TransactionInfo>();
        
        private static Parking _instance;

        private Parking()
        {
        }

        public static Parking GetInstance()
        {
            if (_instance == null)
                _instance = new Parking();
            
            return _instance;
        }
    }
}