﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking p = Parking.GetInstance();
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            
            _withdrawTimer.Interval = Settings.WithdrawInterval;
            _withdrawTimer.Elapsed += (sender, args) =>
            {
                foreach (var vehicle in p.Vehicles)
                {
                    decimal tariff = Settings.TariffDependingOnTransportType[vehicle.VehicleType];
                    decimal fine = tariff;

                    if (vehicle.Balance <= 0)
                        fine = tariff * Settings.FineMultiplier;

                    else if (vehicle.Balance - fine < 0 && vehicle.Balance >= 0)
                        fine = vehicle.Balance + (tariff - vehicle.Balance) * Settings.FineMultiplier;
                    
                    
                    vehicle.Balance -= fine;
                    p.Balance += fine;

                    
                    TransactionInfo tr = new TransactionInfo {Sum = fine, Time = DateTime.Now, VehicleId = vehicle.Id};
                    p.Transactions.Add(tr);
                }
            };

            
            _logTimer.Interval = Settings.LogInterval;
            _logTimer.Elapsed += (sender, args) =>
            {
                var tr = p.Transactions;
                string result = "";
                
                for (int i = p.LastLoggedTransactionIndex; i < p.Transactions.Count; i++)
                {
                    result = "Sum: " + tr[i].Sum + "\n" +
                             "Vehicle ID: " + tr[i].VehicleId + "\n" +
                             "Time: " + tr[i].Time.ToString("HH:mm:ss") + "\n";
                }

                p.LastLoggedTransactionIndex = tr.Count - 1;
                
                _logService.Write(result);
                p.LastTransactions.RemoveRange(0, p.LastTransactions.Count);
            };
            
            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            p.Balance = 0;
            p.VehicleIds.RemoveRange(0, p.VehicleIds.Count);
            p.Vehicles.RemoveRange(0, p.Vehicles.Count);
            p.Transactions.RemoveRange(0, p.Transactions.Count);
            p.LastTransactions.RemoveRange(0, p.LastTransactions.Count);
            p.LastLoggedTransactionIndex = 0;
        }

        public decimal GetBalance()
        {
            return p.Balance;
        }

        public int GetCapacity()
        {
            return p.Capacity;
        }

        public int GetFreePlaces()
        {
            return p.Capacity - p.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return p.Vehicles.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (p.Capacity == p.Vehicles.Count)
                throw new InvalidOperationException("The parking is already full");
            
            Vehicle vehicle1 = p.Vehicles.FirstOrDefault(v => v.Id == vehicle.Id);
            if (vehicle1 != null)
                throw new ArgumentException("The id is not unique");
            
            p.Vehicles.Add(vehicle);
            p.VehicleIds.Add(vehicle.Id);
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = p.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Invalid identifier");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("The car has negative balance");
            
            p.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = p.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Invalid identifier");
            if (sum < 0)
                throw new ArgumentException("Sum can not be negative");

            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            for (int i = p.LastLoggedTransactionIndex; i < p.Transactions.Count; i++)
            {
                if (i >= 0)
                    p.LastTransactions.Add(p.Transactions[i]);
                
                else break;
            }

            return p.LastTransactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
    }
} 