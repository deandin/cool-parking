﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService, IDisposable
    {
        public string LogPath { get; private set; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public void Write(string logInfo)
        {
            using (var sw = new StreamWriter(LogPath, true))
            {
                sw.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("File does not exists");
            
            string result;
            using (var sr = new StreamReader(LogPath))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        public void Dispose()
        {
            File.Delete(LogPath);
        }
    }
}