﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using CoolParking.BL;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL.ConsoleApp
{
    class Program
    {
        private static HttpClient _client = new HttpClient();
        private const string UriApi = "https://localhost:5001/api/";
        static void Main(string[] args)
        {
            TimerService withdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            LogService logService = new LogService(@"F:\PROJECTS\test.txt");
            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            
            MainMenu(parkingService);
        }
        
        private enum MainMenuAction
        {
            ShowParkingBalance = 1,
            ShowCapacity = 2,
            ShowFreeSpaces = 3,
            ShowAllTransactionsBeforeLog = 4,
            ShowAllTransactions = 5,
            TopUpVehicle = 6,
            ShowAllVehicles = 7,
            AddVehicleToParking = 8,
            TakeVehicleFromParking = 9,
            ShowVehicleById = 10,
            Exit = 11,
        }
        
        private const string MenuText = "1. Поточний баланс Паркінгу \n" +
                                         "2. Сума зароблених коштів за поточний період \n" +
                                         "3. Кількість вільних місць на паркуванні \n" +
                                         "4. Усі Транзації паркінгу за поточний період \n" +
                                         "5. Історія Транзакцій \n" +
                                         "6. Поповнити баланс конкретного Тр. засобу \n" +
                                         "7. Список Тр. засобів, що знаходяться на Паркінгу \n" +
                                         "8. Поставити Транспортний засіб на Паркінг \n" +
                                         "9. Забрати Транспортний засіб з Паркінгу \n" +
                                         "10. Показати тр. засіб за ID \n" +
                                         "11. Вихід \n";
        private static void MainMenu(IParkingService parkingService)
        {
            Console.OutputEncoding = Encoding.Unicode;
            while (true)
            {
                Console.WriteLine(MenuText);
                
                int userChoice = Convert.ToInt32(Console.ReadLine());
                if (userChoice == (int) MainMenuAction.Exit)
                {
                    break;
                }

                HandleMainMenuAction((MainMenuAction) userChoice, parkingService);
                
            }
        }
        
        private static void HandleMainMenuAction(MainMenuAction action, IParkingService parkingService)
        {
            string neededId = "";
            HttpResponseMessage response;
            StringContent content;
            switch (action)
            {
                case MainMenuAction.ShowParkingBalance:
                    response = _client.GetAsync(UriApi + "parking/balance").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;

                case MainMenuAction.ShowCapacity:
                    response = _client.GetAsync(UriApi + "parking/capacity").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.ShowFreeSpaces:
                    response = _client.GetAsync(UriApi + "parking/freePlaces").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.ShowAllTransactionsBeforeLog:
                    response = _client.GetAsync(UriApi + "transactions/last").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.ShowAllTransactions:
                    response = _client.GetAsync(UriApi + "transactions/all").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.TopUpVehicle:
                    Console.WriteLine("Введіть ID транспорта, який баланс якого хочете поповнити: ");
                    neededId = Console.ReadLine();
                    Console.WriteLine("Введіть суму, на яку хочете поповнити: ");
                    decimal sum = Convert.ToDecimal(Console.ReadLine());

                    content = new StringContent(neededId + sum, Encoding.UTF8, "application/json");
                    response = _client.PutAsync(UriApi + "transactions/topUpVehicle", content).Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.ShowAllVehicles:
                    response = _client.GetAsync(UriApi + "vehicles/").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;

                case MainMenuAction.AddVehicleToParking:
                    Console.WriteLine("Введіть тип цього транспорту \n" +
                                      "(Легкова = 1, Вантажна = 2, Автобус = 3, Мотоцикл = 4)");
                    int vehicleType = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введіть баланс цього транспорту: ");
                    decimal balance = Convert.ToDecimal(Console.ReadLine());

                    var vehicle = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(),
                        (VehicleType) vehicleType, balance);
                    content = new StringContent(vehicle.ToString(), Encoding.UTF8, "application/json");
                    response = _client.PostAsync(UriApi + "vehicles/", content).Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.ShowVehicleById:
                    Console.WriteLine("Введіть ID транспорта, який хочете побачити: ");
                    neededId = Console.ReadLine();
                    response = _client.GetAsync(UriApi + $"vehicles/{neededId}").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;
                
                case MainMenuAction.TakeVehicleFromParking:
                    Console.WriteLine("Введіть ID транспорта, який хочете забрати з паркінга: ");
                    neededId = Console.ReadLine();
                    response = _client.GetAsync(UriApi + $"vehicles/{neededId}").Result;
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    break;

                
                
                default:
                    Console.WriteLine("Unexpected error");
                    break;
            }
        }
    }
}