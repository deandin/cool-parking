﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : Controller
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [HttpGet]
        [Route("balance")]
        public decimal Balance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet]
        [Route("capacity")]
        public int Capacity()
        {
            return _parkingService.GetCapacity();
        }
        
        [HttpGet]
        [Route("freePlaces")]
        public int FreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}