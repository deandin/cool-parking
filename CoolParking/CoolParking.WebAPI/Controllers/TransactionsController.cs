﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [HttpGet]
        [Route("last")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }
        
        [HttpGet]
        [Route("all")]
        public ActionResult<string> GetAllTransactions()
        {
            string result;
            try
            {
                result = _parkingService.ReadFromLog();
            }
            catch (InvalidOperationException e)
            {
                return NotFound();
            }
            
            return Ok(result);
        }
        
        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult TopUpVehicle(string id, decimal sum)
        {
            if (!Vehicle.IsIdFormatCorrect(id) || sum <= 0)
                return BadRequest();
            
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
                return NotFound();

            _parkingService.TopUpVehicle(id, sum);
            return Ok();
        }
    }
}