﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [HttpGet]
        public IEnumerable<Vehicle> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }
        
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            Vehicle result = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);

            if (!Vehicle.IsIdFormatCorrect(id))
                return BadRequest();

            if (result == null)
                return NotFound();
            
            return Ok(result);
        }
        
        [HttpPost]
        public ActionResult PostVehicle(Vehicle vehicle)
        {
            var t = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicle.Id);
            if (t != null || !Vehicle.IsIdFormatCorrect(vehicle.Id) || vehicle.VehicleType < 0 || 
                (int)vehicle.VehicleType >= 4 || vehicle.Balance < 0)
                return BadRequest();
            
            _parkingService.AddVehicle(vehicle);

            return CreatedAtAction(nameof(GetVehicleById), new { id = vehicle.Id}, vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsIdFormatCorrect(id))
                return BadRequest();

            if (_parkingService.GetVehicles().FirstOrDefault(v => v.Id == id) == null)
                return NotFound();
            
            _parkingService.RemoveVehicle(id);

            return NoContent();
        }
    }
}